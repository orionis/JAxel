package com.dragon.jaxel.bittorrent.bencode;

public class BencodeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8538876113386672025L;

	public BencodeException() {
		// TODO Auto-generated constructor stub
	}

	public BencodeException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public BencodeException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public BencodeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public BencodeException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
