/**
 *Copyright 2013 by dragon.
 *
 *File name: BencodeValue.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-8 上午9:34:07
 *Description: 
 */
package com.dragon.jaxel.bittorrent.bencode;

/**
 * Bencode 数据类型
 * 
 * @author dragon8
 * 
 */
public class BencodeValue {
	private byte[] bytes = null;

	/**
	 * 获取原始二进制数据
	 * 
	 * @return
	 */
	public byte[] getBytes() {
		return bytes;
	}

	protected BencodeValue(byte[] bytes) {
		super();
		this.bytes = bytes;
	}

	public BencodeDictionary getAsDictionary() {
		return (BencodeDictionary) this;
	}

	public BencodeList getAsBencodeList() {
		return (BencodeList) this;
	}

	public BencodeString getAsBencodeString() {
		return (BencodeString) this;
	}

	public BencodeNumber getAsBencodeNumber() {
		return (BencodeNumber) this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return new String(getBytes());
	}

}
