/**
 *Copyright 2013 by dragon.
 *
 *File name: BencodeString.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-8 上午9:38:45
 *Description: 
 */
package com.dragon.jaxel.bittorrent.bencode;

/**
 * Bencode 串
 * @author dragon8
 * 
 */
public class BencodeString extends BencodeValue {

	public BencodeString(byte[] bytes) {
		super(bytes);
		// TODO Auto-generated constructor stub
	}

	public BencodeString(String data) {
		super(data.getBytes());
		// TODO Auto-generated constructor stub
	}

	public String getData() {
		return new String(getBytes());
	}
}
