/**
 *Copyright 2013 by dragon.
 *
 *File name: PieceManager.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-9 下午6:03:27
 *Description: 
 */
package com.dragon.jaxel.bittorrent;

import java.util.ArrayList;

/**
 * 管理文件读写
 * 
 * @author dragon8
 * 
 */
public class PieceManager {

	transient Piece[] pieces;

	BitSet bitSet;

	/**
	 * @return the pieces
	 */
	Piece[] getPieces() {
		return pieces;
	}

	/**
	 * 
	 */
	public PieceManager() {
		// TODO Auto-generated constructor stub
	}

	void init(long fileOffset, long fileLength, int pieceSize) {
		long fOffset = fileOffset % pieceSize;
		fOffset = -fOffset;
		ArrayList<Piece> list = new ArrayList<Piece>();
		int i = 0;
		while (fOffset < fileLength) {
			Piece piece = new Piece(i++);
			piece.init(fOffset, pieceSize);
			list.add(piece);
			fOffset += pieceSize;
		}
		pieces = list.toArray(new Piece[list.size()]);
		bitSet = new BitSet(pieces.length);
	}
}
