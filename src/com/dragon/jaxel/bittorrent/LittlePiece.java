/**
 *Copyright 2013 by dragon.
 *
 *File name: Piece16K.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-9 下午5:00:08
 *Description: 
 */
package com.dragon.jaxel.bittorrent;

/**
 * 16K片断
 * 
 * @author dragon8
 * 
 */
public class LittlePiece {

	/**
	 * 表示在元信息中的片段索引
	 */
	final int pieceIndex;

	/**
	 * 在一个大片段中的偏移
	 */
	final int pieceOffset;
	/**
	 * 对应文件的偏移
	 */
	final long fileOffset;

	/**
	 * 片段长度，一般为16K
	 */
	final int length;

	/**
	 * 是否已经完成写操作
	 */
	boolean isFinished = false;

	/**
	 * 是否正在请求
	 */
	boolean isRequesting = false;

	/**
	 * @return the isFinished
	 */
	boolean isFinished() {
		return isFinished;
	}

	/**
	 * @param isFinished
	 *            the isFinished to set
	 */
	void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public LittlePiece(int pieceIndex, int pieceOffset, long fileOffset,
			int length) {
		super();
		this.pieceIndex = pieceIndex;
		this.pieceOffset = pieceOffset;
		this.fileOffset = fileOffset;
		this.length = length;
	}

}
