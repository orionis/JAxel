/**
 *Copyright 2013 by dragon.
 *
 *File name: Downloader.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-7 下午9:38:26
 *Description: 抽象类,为单个文件下载提供统一接口
 */
package com.dragon.jaxel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.InvocationTargetException;

/**
 * @author dragon8
 * 
 */
public interface Downloader {

	/**
	 * 所有下载线程是否已结束，有可能被中断，不一定已下载完成
	 * 
	 * @return
	 */
	public boolean isDownLoading();

	/**
	 * 是否文件已经下载完成
	 * 
	 * @return
	 */
	public boolean isCompleted();

	/**
	 * @return
	 */
	public String getSavePathString();

	/**
	 * 获取文件长度
	 * 
	 * @return
	 */
	public long getSize();

	/**
	 * 获取总进度
	 * 
	 * @return
	 */
	public long getProgress();

	/**
	 * 删除状态文件
	 */
	public void deleteSateFile();

	/**
	 * 保存下载信息到文件
	 * 
	 * @throws IOException
	 */
	public void saveState() throws IOException;

	/**
	 * 读取上次下载信息
	 * 
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void loadState(ObjectInputStream inputStream)
			throws FileNotFoundException, IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException;

	/**
	 * 开始下载
	 * 
	 * @return 操作是否成功
	 * @throws IOException
	 */
	public boolean start() throws IOException;

	/**
	 * 开始下载
	 * 
	 * @param connectionCount
	 *            指定连接数
	 * @return 操作是否成功
	 * @throws IOException
	 */
	public boolean start(int connectionCount) throws IOException;

	/**
	 * 停止下载
	 */
	public void stop();

	/**
	 * 暂停下载
	 */
	public void pause();

	/**
	 * 继续下载
	 * 
	 * @return
	 * @throws IOException
	 */
	public boolean resume() throws IOException;

	/**
	 * 服务器是否支持断点下载
	 * 
	 * @return
	 */
	public boolean isSupportRange();

	/**
	 * 获取下载协议的字符串表示
	 * 
	 * @return "http","ftp"...
	 */
	public String getProtocol();
}
